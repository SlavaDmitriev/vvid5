class MyTest(unittest.TestCase):
	"""
	sdfsdfsdf
	sdfsdfsdfsdf
	sdfsdfsdf
	"""
    def test_Nikita_Mat(self):
    """
    Функция проверяет достоверность работы программы, всё лиработает корректно.
    Она включает в себя два значения, которых подставляет в основную функцию и
    проверяет результат со своим изначально заложенным, если всё сходится,
    программа сообщает об успешном выполнении работы.
    self:
        NAME:    NIKITA.
        LASTNAME:    MAT.
    LOGIN:    NIKIM.

    Returns:    NIKIM successfully registered.
    """
        res = check_the_name("NIKITA","MAT")
        self.assertEqual(res, "NIKIM successfully registered")


    def test_Slava_Dm(self):
    """
    Функция проверяет достоверность работы программы, всё лиработает корректно.
    Она включает в себя два значения, которых подставляет в основную функцию и
    проверяет результат со своим изначально заложенным, если всё сходится,
    программа сообщает об успешном выполнении работы.
    self:
        NAME:    SLAVA.
        LASTNAME:    D.
    LOGIN:    SD.

    Returns:    SD successfully registered.
    """
        res = check_the_name("SLAVA","D")
        self.assertEqual(res, "SD successfully registered")


    def test_Artur_Har(self):
    """
    Функция проверяет достоверность работы программы, всё лиработает корректно.
    Она включает в себя два значения, которых подставляет в основную функцию и
    проверяет результат со своим изначально заложенным, если всё сходится,
    программа сообщает об успешном выполнении работы.
    self:
        NAME:    ARTUR.
        LASTNAME:    HAR.
    LOGIN:    AH.

    Returns:    AH successfully registered.
    """
        res = check_the_name("ARTUR","HAR")
        self.assertEqual(res, "AH successfully registered")



    def test_Nar_Z(self):
    """
    Функция проверяет достоверность работы программы, всё лиработает корректно.
    Она включает в себя два значения, которых подставляет в основную функцию и
    проверяет результат со своим изначально заложенным, если всё сходится,
    программа сообщает об успешном выполнении работы.
    self:
        NAME:    NAR.
        LASTNAME:    ZK.
    LOGIN:    NARZ.

    Returns:    NARZ successfully registered.


    Однако, если на вход поступят такие значения, как:
    self:
        NAME:    NAR.
        LASTNAME:    ZK.
    LOGIN:    NARK.

    Returns:    'NARK successfully registered' != 'NARZ successfully registered'.

    Raises:    AssertionError.
    """
        res = check_the_name("NAR","ZK")
        self.assertEqual(res, "NARZ successfully registered")